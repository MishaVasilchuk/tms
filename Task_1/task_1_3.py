a = input('Please, enter the length of the edge of the cube: ')
a = int(a)
v = a**3
s = 4 * (a**2)
print('Lateral surface of a cube = {0} and cube volume = {1}'.format(v, s))
