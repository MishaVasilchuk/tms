from statistics import geometric_mean
a = input('Please, enter variable a: ')
b = input('Please, enter variable b: ')
a = float(a)
b = float(b)
arithmetic_mean_result = (a + b) / 2
geometric_mean_result = geometric_mean([a, b])
print('Arithmetic mean of {0} and {1} = {2}'.format(a, b, arithmetic_mean_result))
print('Geometric mean result of {0} and {1} = {2}'.format(a, b, geometric_mean_result))
