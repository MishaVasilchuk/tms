string_user = str(input('Enter your string: '))
if len(string_user) > 5:
    string_second = string_user[2]
    print('A string equal to the third character of the input string: ', string_second)
    string_third = string_user[(len(string_user) - 2)]
    print('A string equal to the penultimate character of the entered string: ', string_third)
    string_fourth = string_user[:5]
    print('A string equal to the first five characters of the input string: ', string_fourth)
    i = len(string_user)
    string_fifth = string_user[0:(i-2)]
    print('A string equal to the input string without the last two characters: ', string_fifth)
    string_sixth = string_user[::2]
    print('A string equal to all elements of the input string with even indices: ', string_sixth)
else:
    print('Error! Enter string more 5 symbols!')
