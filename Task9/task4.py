def arg_reverser(func):
    def wrapper(*args, **kwargs):
        print(args, kwargs)
        args = args[::-1]
        kwargs = {key: kwargs[key] for key in list(kwargs.keys())[::-1]}
        return func(*args, **kwargs)
    return wrapper


@arg_reverser
def full_function(*args, **kwargs):
    print('aaa It\'s reversed!', args, kwargs)


full_function(1, 2, 3, 4, 5, 6, 7, 8, a=1, b=2)
