dict_1 = lambda **kwargs: {key * 2: value for key, value in kwargs.items()}

print(dict_1(a=1, b=2, c=3, d=4, e=5, f=6, g=7, k=8))
