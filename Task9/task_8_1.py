string = ['hello', 'python', 'programmer']
func = lambda names: ['{0} - {1}'.format(i, name) for i, name in enumerate(string)]
print(func(string))