def preparator(func):
    def wrapper(numbers):
        print('start wrapper')
        print(numbers)
        print('start {0}'.format(func))
        func(list(filter(lambda x: x % 2 != 0, numbers)))
        print('end function')
        print(numbers)
        return numbers

    return wrapper


@preparator
def sum_numbers(numbers):
    print(numbers)
    return print(sum(numbers))


numbers = [1, 2, 3, 4, 5, 6, 7, 8]
print(sum_numbers(numbers))
