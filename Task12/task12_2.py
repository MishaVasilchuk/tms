class People:
    __counter = 0
    max_food_portion = None
    people_speak = None

    def __init__(self, name, age, weight, height, gender, health='Good'):
        self.name = name
        self.age = age
        self.weight = weight
        self.height = height
        self.gender = gender
        self.__health = health or 'Good'
        People.__counter += 1

    def run(self):
        print(f'{self.name} run!')

    def eating(self, food_portion):
        if self.max_food_portion is not None and food_portion > self.max_food_portion:
            print(f"{self.name} cannot eat so much food!")
        else:
            print(f'Eat {food_portion} portions!')

    def jump(self):
        print(f'{self.name} jump!')

    def change_weight(self, d_weight=None):
        d_weight = d_weight or 0.2
        self.weight += d_weight

    def change_height(self, d_height=None):
        d_height = d_height or 0.2
        self.height += d_height

    def change_age(self, n_age=None):
        if n_age:
            n_age = n_age
        else:
            print('Year added automatically!')
            n_age = 1
        self.age += n_age

    @classmethod
    def get_counter(cls):
        print(f'People\'s count: {cls.__counter}!')

    def get_health(self):
        return self.__health

    def speaking(self):
        people_speak = self.people_speak or '<Unindefined speak language>'
        print(people_speak)


class EnglishPeople(People):
    max_food_portion = 12
    people_speak = 'English'

    def __init__(self, language, name, age, weight, height, gender, health):
        super().__init__(name, age, weight, height, gender, health)
        self.language = language

    def speakeneglish(self):
        print(self.name, 'can speak english!')

    def change_weight(self, d_weight=None):
        d_weight = d_weight or 0.5
        self.weight += d_weight


class SingerPeople(People):
    max_food_portion = 17
    people_speak = 'English'
    singerpeople_count = 0

    def __init__(self, language, name, age, weight, height, gender, health):
        super().__init__(name, age, weight, height, gender, health)
        self.language = language
        SingerPeople.singerpeople_count += 1

    def sing_songs(self):
        print(self.name, 'can sing songs!')

    @classmethod
    def get_counter(cls):
        print(f'People who can sing: {cls.singerpeople_count}!')


class Actor(People):
    max_food_portion = 19
    people_speak = 'English'

    def __init__(self, language, name, age, weight, height, gender, health):
        super().__init__(name, age, weight, height, gender, health)
        self.language = language

    def play_a_role(self):
        role = input('role:')
        print(self.name, f'play\'s a {role} role!')


misha = People('Misha', 35, 85, 190, 'male', 'Very Good')
kostya = People('Kostya', 38, 87, 170, 'male')

misha.run()
kostya.jump()

# Misha
print(misha.name)
print(misha.name, misha.weight, 'kg')
print(misha.name, misha.height, 'sm')
misha.change_weight(10)
misha.change_height()
print('\nAfter holidays\n')
print(misha.name, misha.weight, 'kg')
print(misha.name, misha.height, 'sm\n')
print(misha.age)
misha.change_age()
print(misha.age)
print(misha.name, 'health is', misha.get_health(), '!')

# Kostya
print()
print(kostya.name)
print(kostya.name, kostya.weight, 'kg')
print(kostya.name, kostya.height, 'sm')
kostya.change_weight(30)
kostya.change_height(5)
print('\nAfter holidays\n')
print(kostya.name, kostya.weight, 'kg')
print(kostya.name, kostya.height, 'sm')

People.get_counter()

victor = EnglishPeople('English', 'Victor', 356, 85, 190, 'male', 'Very Good Health')
tom = SingerPeople('English', 'Tom', 356, 85, 190, 'male', 'Very Good Health')
mihail = Actor('English', 'Misha', 356, 85, 190, 'male', 'Very Good Health')

victor.speakeneglish()
victor.speaking()
victor.eating(12)
print(dir(EnglishPeople))
print(victor.weight)
victor.change_weight()
print(victor.weight)
tom.sing_songs()
tom.speaking()
tom.eating(38)
mihail.play_a_role()
mihail.eating(17)


