dict_1 = {'test': 'test_value', 'europe': 'eur', 'dollar': 'usd', 'ruble': 'rub'}
new_dict = {}
for key in dict_1.keys():
    key_length = len(key)
    new_key = f'{key}{key_length}'
    new_key = key+ str(key_length)
    new_dict[new_key] = dict_1[key]
print(new_dict)

keys = list(dict_1.keys())
new_dict = {}
i = 0
while i < len(keys):
    key = keys[i]
    key_length = len(key)
    new_key = key + str(key_length)
    new_dict[new_key] = dict_1[key]
    i += 1
print(new_dict)