list_a = [11, 18, 50, 70, 55, 15, 24]
count = 0
for item in list_a:
    if item % 2 == 0:
        count += 1
print('1st method', count)

count = 0
count_list = len(list_a)
n = 0
while n < count_list:
   if list_a[n] % 2 == 0:
       count += 1
   n += 1
print('2nd method', count)
