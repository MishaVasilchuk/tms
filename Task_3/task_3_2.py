print('=======\nCongratulations on your wedding!\n=======')
while True:
    try:
        number = int(input('Enter the number of guests to select the optimal establishment: '))
        if number >= 50:
            print('The optimal establishment for {0} guests is a restaurant'.format(number))
        elif number >= 20 and number < 50:
            print('The optimal establishment for {0} guests is a cafe'.format(number))
        elif number < 20:
            print('The optimal establishment for {0} guests is a home'.format(number))
        break
    except Exception as e:
        print('Enter the number of guests')