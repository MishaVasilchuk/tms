import math
string = input('Please, enter your string: ')
str_centre = math.ceil(len(string) / 2) - 1
print('Centred symbol: {0}'.format(string[str_centre]))
if string[str_centre].lower() == string[0].lower():
    print('String between 1st and last symbol: {0}'.format(string[1:(len(string) - 1)]))