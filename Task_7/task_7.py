def conversion_sm_inches(value):
    inche = value / 2.54

    return inche


def conversion_inches_sm(value):
    sm = value * 2.54

    return sm


def conversion_miles_km(value):
    km = value * 1.609

    return km

def conversion_km_miles(value):
    miles = value / 1.609

    return miles


def conversion_pounds_kg(value):
    kg = value * 2.2046

    return kg

def conversion_kg_pounds(value):
    pounds = value / 2.2046

    return pounds


def conversion_ounce_grams(value):
    grams = value / 0.035274

    return grams


def conversion_grams_ounce(value):
    ounce = value * 0.035274

    return ounce


def conversion_gallons_liters(value):
    liters = value * 3.79

    return liters


def conversion_liters_gallons(value):
    gallons = value / 3.79

    return gallons


def conversion_pints_liters(value):
    liters = value * 0.568

    return liters


def conversion_liters_pints(value):
    pints = value / 0.568

    return pints


while True:
    choice = input('''Select conversion:
1. Inches to Centimeters
2. Centimeters to inches
3. Miles to kilometers
4. Kilometers to miles
5. Pounds to kilograms
6. Kilograms to pounds
7. Ounces to grams
8. Grams to ounces
9. Gallons to liters
10. Liters to Gallons
11. Pints to liters
12. Liters to pints
    ''')

    if choice.isdigit():
        choice = int(choice)
        if choice == 1:
            n = int(input('Enter value:'))
            print(conversion_sm_inches(n))
        elif choice == 2:
            n = int(input('Enter value:'))
            print(conversion_inches_sm(n))
        elif choice == 3:
            n = int(input('Enter value:'))
            print(conversion_miles_km(n))
        elif choice == 4:
            n = int(input('Enter value:'))
            print(conversion_km_miles(n))
        elif choice == 5:
            n = int(input('Enter value:'))
            print(conversion_pounds_kg(n))
        elif choice == 6:
            n = int(input('Enter value:'))
            print(conversion_kg_pounds(n))
        elif choice == 7:
            n = int(input('Enter value:'))
            print(conversion_ounce_grams(n))
        elif choice == 8:
            n = int(input('Enter value:'))
            print(conversion_grams_ounce(n))
        elif choice == 9:
            n = int(input('Enter value:'))
            print(conversion_gallons_liters(n))
        elif choice == 10:
            n = int(input('Enter value:'))
            print(conversion_liters_gallons(n))
        elif choice == 11:
            n = int(input('Enter value:'))
            print(conversion_pints_liters(n))
        elif choice == 12:
            n = int(input('Enter value:'))
            print(conversion_liters_pints(n))
        elif choice == 0:
            break
    else:
        print('Enter correct number!')