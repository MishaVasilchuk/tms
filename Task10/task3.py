import csv
from datetime import datetime, date
from typing import Union

from dateutil import parser
from random import randint
from faker import Faker

fake = Faker()

data = [
    f'{" ".join(str(fake.date()).split("-"))}\n' for i in range(1000)
]

with open('dates.txt', 'w') as file:
    file.writelines(data)

min_date: Union[None, date] = None
with open('dates.txt') as file:
    while True:
        line = file.readline()
        if not line:
            break
        year, month, day = line.split(' ')
        d = date(month=int(month), day=int(day), year=int(year))
        if min_date is None or min_date > d:
            min_date = d

print(min_date)
