import csv
from datetime import datetime
from dateutil import parser
from random import randint
from faker import Faker

fake = Faker()

data = [
    {'date': fake.date_time_between(start_date='-15d', end_date='+10d'),
     'city': fake.city(),
     'temperature': randint(-20, 40),
     'wind speed': randint(0, 150)
     } for i in range(100)
]

data += [
    {'date': fake.date_time_between(start_date='-15d', end_date='+10d'),
     'city': 'Minsk',
     'temperature': randint(-20, 40),
     'wind speed': randint(0, 150)
     } for i in range(43)
]

with open('weather.csv', 'w') as file:
    writer = csv.DictWriter(file, fieldnames=data[0].keys())
    writer.writeheader()
    writer.writerows(data)

wind_speed_list = []
with open('weather.csv') as file:
    for row in csv.DictReader(file):
        date = parser.parse(row['date'])
        timedelta = (date.now() - date).total_seconds() / 60 / 60 / 24
        print(timedelta)
        if row['city'] == 'Minsk' and 0 < timedelta <= 7:
            wind_speed_list.append(int(row['wind speed']))

print('Average wind speed in Minsk:', sum(wind_speed_list) / len(wind_speed_list))