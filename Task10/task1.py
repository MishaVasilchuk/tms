import csv
from random import randint
from faker import Faker
fake = Faker()


data = [
    {'name': fake.first_name(), 'surname': fake.last_name(), 'age': randint(1, 149)} for i in range(10000)
]


with open('data.csv', 'w') as file:
    writer = csv.DictWriter(file, fieldnames=data[0].keys())
    writer.writeheader()
    writer.writerows(data)


formated_data = {
    (1, 12): 0,
    (13, 18): 0,
    (19, 25): 0,
    (26, 40): 0,
    (40, None): 0
}


with open('data.csv') as file:
    for row in csv.DictReader(file):
        age = int(row['age'])
        for start, end in formated_data.keys():
            if age >= start and (end is None or age <= end):
                formated_data[(start, end)] += 1
                break

for key, value in formated_data.items():
    print(key, value)