class Pet:
    max_jump_height = None
    animal_voice = None
    __counter = 0

    def __init__(self, name, age, master, weight, height):
        self.name = name
        self.age = age
        self.master = master
        self.weight = weight
        self.height = height
        Pet.__counter += 1

    @staticmethod
    def run():
        print('Run!')

    def jump(self, jump_height):
        if self.max_jump_height is not None and jump_height > self.max_jump_height:
            print(f"{self.__class__.__name__}s cannot jump so high ({jump_height}m)!")
        else:
            print(f'Jump {jump_height} meters!')

    def birthday(self):
        self.age += 1

    def change_weight(self, d_weight=None):
        d_weight = d_weight or 0.2
        self.weight += d_weight

    def change_height(self, d_height=None):
        d_height = d_height or 0.2
        self.height += d_height

    def voice(self):
        animal_voice = self.animal_voice or '<Unindefined animal>'
        print(animal_voice)

    @classmethod
    def get_count(cls):
        print(f'Pets count: {cls.__counter}')


class Dog(Pet):
    max_jump_height = 0.5
    animal_voice = 'Bark!'


class Cat(Pet):
    max_jump_height = 2
    animal_voice = 'Meow!'


class Parrot(Pet):
    max_jump_height = 0.05
    animal_voice = 'ЧЫКЧЫРЫК!'

    def __init__(self, species, name, age, master, weight, height):
        super().__init__(name, age, master, weight, height)
        self.species = species

    def fly(self):
        if self.weight <= 0.1:
            print('Fly!')
        else:
            print('This parrot can\'t fly!')

    def change_weight(self, d_weight=None):
        d_weight = d_weight or 0.05
        self.weight += d_weight

    def change_height(self, d_height=None):
        d_height = d_height or 0.05
        self.height += d_height


print(Cat.max_jump_height)

cat = Cat(name='Kotek', age=3, master='Kotek master', height=6, weight=4)
dog = Dog(name='Sobeken', age=3, master='Sobeken master', height=8, weight=7)
parrot1 = Parrot(name='Чыкчырык 1', age=3, master='Ptichken master', height=0.2, weight=0.05, species=3)
parrot2 = Parrot(name='Чыкчырык 2', age=67, master='Ptichken master', height=0.1, weight=0.01, species=2)

# cat
print('\nCat:')
print(cat.name)
cat.jump(1.9)
cat.run()
cat.voice()
print(cat.weight)
print(cat.height)
cat.change_weight(1)
cat.change_height()
print(cat.weight)
print(cat.height)

# dog
print('\nDog:')
dog.jump(0.6)
dog.run()
dog.voice()
print(dog.weight)
print(dog.height)
dog.change_weight(1)
dog.change_height(2)
print(dog.weight)
print(dog.height)

# parrot
print('\nParrot:')
parrot1.jump(0.06)
parrot1.run()
parrot1.voice()
parrot1.fly()
print(parrot1.weight)
print(parrot1.height)
parrot1.change_weight()
parrot1.change_height(0.01)
print(parrot1.weight)
print(parrot1.height)
parrot1.fly()

print('\nParrot 2:')
parrot2.jump(0.02)
parrot2.run()
parrot2.voice()
parrot2.fly()
print(parrot2.weight)
print(parrot2.height)
parrot2.change_weight()
parrot2.change_height(0.01)

# Get list of all class object attributes
print('|----------|')
for attr in dir(cat):
    if not attr.startswith('__'):
        print(attr)

Pet.get_count()
Dog.get_count()
Cat.get_count()
Parrot.get_count()
