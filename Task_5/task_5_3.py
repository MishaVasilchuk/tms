def dividers(n):
    dividers_list = []
    for i in range(1, n):
        if n % i == 0:
            dividers_list.append(i)

    return dividers_list


dividers_sum_map = {}
for m in range(200, 301):
    all_dividers = dividers(m)
    dividers_sum = sum(all_dividers)

    if dividers_sum not in dividers_sum_map:
        dividers_sum_map[dividers_sum] = [m]
    else:
        dividers_sum_map[dividers_sum].append(m)

for numbers in dividers_sum_map.values():
    if len(numbers) > 1:
        print(*numbers)