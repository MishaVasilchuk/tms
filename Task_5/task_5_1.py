while True:
    x = input('Enter value x:')               # Принимаем два значения и оператор вычисления
    y = input('Enter value y:')
    operator = input('Enter operation sign:')

    need_to_break = False    # Добавляем флаг ошибки.

    if not x.isdigit():
        print('Enter number x!')      # Если x или y не число, значит выводим ошибку и поворачиваем флаг
        need_to_break = True
    if not y.isdigit():
        print('Enter number y!')
        need_to_break = True
    try:
        x = int(x)                 # Пробуем перевести x и y к int
        y = int(y)
    except Exception:
        need_to_break = True       # В противном случае ловим ошибку и переворачиваем флаг

    supported_operators = ['+', '-', '/', '*']    # Создаем массив подходящих знаков

    if operator not in supported_operators:    # Если оператор не в списке операторов, значит печатаем ошибку
        print('Enter correct operator!')       # и переворачиваем флаг
        need_to_break = True

    if operator == '/' and y == 0:      # Если оператор деления и y = 0, печатаем ошибку и переворачиваем флаг
        print('Invalid zero division')
        need_to_break = True

    if need_to_break:              # если need_to_break = True, значит была ошибка и мы спрашиваем пользоавтеля,
        decision = input('Do you want to retry? (Y/N)')    # Хочет ли он попробовать еще раз
        if decision.lower() == 'y':
            continue
        else:
            break

    if operator == '+':     # Основные действия
        z = x + y
    elif operator == '-':
        z = x - y
    elif operator == '/':
        z = x / y
    elif operator == '*':
        z = x * y

    print('{0} {1} {2} = {3}'.format(x, operator, y, z))  # Вывод результата