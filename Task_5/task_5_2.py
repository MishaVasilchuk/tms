a = input('Enter number:')
summator = 0
mult = 1
for item in range(len(a)):
    elem = a[item]
    if elem.isdigit():
        summator += int(elem)
        mult *= int(elem)
print('', summator, '\n', mult)